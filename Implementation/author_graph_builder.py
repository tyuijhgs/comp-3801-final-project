from DBParser import Parser
from DBParser import DB_PATH
from CitationRank import Adjacancy_graph, pagerank
from CreateArticleGraph import article_adjacancy_in
import pprint
import pdb
import json
import gc
#from Settings import FILE_MAPPING_ARTICLE_ID_TO_AUTHOR, FILE_ADJACANCY_LIST_AUTHORS, FILE_AUTHOR_VECTOR, FILE_MAPPING_AUTHOR_TO_ARTICLE
from Settings import *
from Utils import *

def build_basic_graphs():
    p = Parser(DB_PATH)
    p.openFile()
    timer = Timer()

    authors_in_article = dict()
    author_cited = dict()
    article_to_title = dict()

    print "{0}: building article to authors and authors citations out edges".format(timer.delta())
    count = 0
    for article in p.parsedItems():
        article_id = None
        authors    = []
        citations  = []
        count += 1
 
        if not article.get('id',False):
            continue

        article_id = article['id']
        authors    = article.get('authors',['unknown {0}'.format(count)])
        citations  = article.get('citations',[])


        article_to_title[article_id] = article.get('title','unknown {0}'.format(count))
        authors_in_article[article_id] = authors
        
        for author in authors:
            author_cited[author] = author_cited.get(author,set()).union(citations)

    p.closeFile()
    
    print "{1}: saving : {0} ".format(FILE_ARTICLE_ID_TO_TITLE_MAP,timer.delta())
    saveToJsonFile(FILE_ARTICLE_ID_TO_TITLE_MAP,article_to_title)

    print "{1}: saving : {0} ".format(FILE_MAPPING_ARTICLE_ID_TO_AUTHOR,timer.delta())
    with open(FILE_MAPPING_ARTICLE_ID_TO_AUTHOR,'w') as f:
        f.write(json.dumps({key:list(item) for key,item in authors_in_article.iteritems()}))

    print "{1}: saving : {0}".format(FILE_MAPPING_AUTHOR_TO_ARTICLE,timer.delta())
    with open(FILE_MAPPING_AUTHOR_TO_ARTICLE,'w') as f:
        f.write(json.dumps({key:list(item) for key,item in author_cited.iteritems()}))
    return (authors_in_article,author_cited)

def build_adjacancy_graph(authors_in_article,author_cited):
    timer = Timer()
    author_graph = dict()

    print "{0}: building author graph".format(timer.delta())
    for author in author_cited.iterkeys():
        list_of_cited_articles = author_cited[author]
        for article in list_of_cited_articles:
            author_graph[author] = author_graph.get(author,set()).union(set(authors_in_article.get(article,[])))

    print "{0}: building author graph inverse".format(timer.delta())
    author_graph = inverseAdjacencyMatrix(author_graph)

    print "{0}: removing self loops".format(timer.delta())
    for author in author_graph.iterkeys():
        if author in author_graph[author]:
            author_graph[author].remove(author)

    print "{1}: saving :{0} ".format(FILE_ADJACANCY_LIST_AUTHORS,timer.delta())
    with open(FILE_ADJACANCY_LIST_AUTHORS,'w') as f:
        f.write(json.dumps({key:list(item) for key,item in author_graph.iteritems()}))

    return author_graph

'''
Only update in initialization
'''

def build_author_graph_vector():

    timer = Timer()

    author_graph = loadFromJsonFile(FILE_ADJACANCY_LIST_AUTHORS)


    #pprint.pprint(author_graph)
    print "{0}: building graph".format(timer.delta())
    author_graph = addTeleportNode(author_graph)
    g = Adjacancy_graph(author_graph)

    # free up memory
    del author_graph
    gc.collect()

    print "{0}: applying page rank".format(timer.delta())
    b = pagerank(g)

    author_vector = {key:b.item(g.map_to_index(key)) for key in g.mapped_keys()}

    with open(FILE_AUTHOR_VECTOR,'w') as f:
        f.write(json.dumps(author_vector))

    return author_vector


'''
files to be updated:
    aurthor ranking vector : only update on new DB
    basic graphs : update on change of size
    article-graphs   : update on change of size
'''
def build_article_vector_base_authors(funct,save_file, article_adjacancy = None):
    timer = Timer()
    authors_in_article = None
    author_vector = None
    bias_vector = None
   
    # page rank of authors
    author_vector = loadFromJsonFile(FILE_AUTHOR_VECTOR)

    # the article graph that we are working with
    authors_in_article = loadFromJsonFile(FILE_MAPPING_ARTICLE_ID_TO_AUTHOR)
    
    # vector that mapped articles from authors to an author ranking
    bias_vector = vector_mapping_factory(authors_in_article,author_vector,funct)

    del authors_in_article
    gc.collect()

    #article_adjacancy = article_adjacancy_in() 
    if (article_adjacancy == None):
        article_adjacancy = loadFromJsonFile(FILE_ARTICLE_ADJECENCY_IN)

    g = Adjacancy_graph(article_adjacancy,bias_vector)
    b = pagerank(g)
   
    article_rank =  {key:b.item(g.map_to_index(key)) for key in g.mapped_keys() }
    
    print "{0} : saving : {1}".format(timer.delta(),save_file)
    saveToJsonFile(save_file,article_rank)
    return article_rank


        
if __name__ == "__main__":
    pass
