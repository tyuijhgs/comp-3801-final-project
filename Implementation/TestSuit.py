from Settings import *
from Utils import *
from pprint import pprint
import os
import itertools
import csv
import logging
import pdb

def getTopN(n,v):
    return sorted(v.items(),key=lambda x:x[1])[::-1][:n]

def getLast(string):
    return os.path.basename(string)
    return string.split('/')[-1]
    
'''
finds all similarities between cosine
'''
def cosineCases():
    paths = VECTOR_LIST
    logging.info('Writing to : {}'.format(FILE_CSV_COSINE))
    with open(FILE_CSV_COSINE,'wb') as f: 
        writer = csv.writer(f)
        for a,b in combinations(paths,2):
            print a,b
            c = cosineDistance(cleanDummy(loadFromJsonFile(a)),cleanDummy(loadFromJsonFile(b)))
            writer.writerow([getLast(a),getLast(b),c])

def cleanDummy(a):
    try:
        a.pop(u'111111')
    except Exception:
        print 'invalid mapping, make sure you update all documents'
    finally:
        return a

'''
finds the fuct h-index value at the top n articles
'''
def indexMatching(N,fileName,funct):
    paths = VECTOR_LIST
    hIndex = loadFromJsonFile(FILE_AUTHOR_H_INDEX)
    articleToAuthor = loadFromJsonFile(FILE_MAPPING_ARTICLE_ID_TO_AUTHOR)
    titleMapper = loadFromJsonFile(FILE_ARTICLE_ID_TO_TITLE_MAP)

    logging.info('Writing to : {}'.format(fileName))
    with open(fileName,'wb') as f: 
        writer = csv.writer(f)
        for item in paths:
            writer.writerow([getLast(item)])
            writer.writerow(['Article','Rank value','Rank','H-Index',funct.__name__])
            for row in helperMatch(titleMapper,articleToAuthor,hIndex,getTopN(N,cleanDummy(loadFromJsonFile(item))),funct):
                writer.writerow(row)

'''
helper function for indexMatching
this is the part that matches the index
'''
def helperMatch(titleMapper,articleToAuthor,hIndex,vector,funct):
    temp = []
    count = 1
    for article, rank in vector:
        h_i = funct([hIndex[author] for author in articleToAuthor[article]])
        temp.append([titleMapper[article].encode("utf-8"),"%.7f"%rank,count,h_i])
        count +=1
    return temp

def topHindex(N):
    hIndex = loadFromJsonFile(FILE_AUTHOR_H_INDEX)

    output = open(FILE_HINDEX_TOP_N, "wb")
    writer = csv.writer(output)
    writer.writerow(['Author','Hindex value','Rank'])

    count = 1
    for author, authorHI in getTopN(N, hIndex):
        writer.writerow([author.encode("utf-8"), authorHI, count])
        count += 1

    output.close()

def topAuthor(N):
    authorRanking = loadFromJsonFile(FILE_AUTHOR_VECTOR)
    authorRanking = cleanDummy(authorRanking)

    output = open(FILE_AUTHOR_TOP_N, "wb")
    writer = csv.writer(output)
    writer.writerow(['Author','Rank value','Rank'])

    count = 1
    for author, rank in getTopN(N, authorRanking):
        writer.writerow([author.encode("utf-8"), "%.7f"%rank, count])
        count += 1

    output.close()

'''
this is the test data suite
'''
if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
    logging.info('Cosine')
    cosineCases()
    logging.info('Max H Index')
    indexMatching(TOP_N,FILE_CSV_INDEX_MATCH_MAX,max)
    logging.info('AVG H Index')
    indexMatching(TOP_N,FILE_CSV_INDEX_MATCH_AVG,avg)
    logging.info('MIN H Index')
    indexMatching(TOP_N,FILE_CSV_INDEX_MATCH_MIN,min)

    logging.info('Top N hIndex')
    topHindex(TOP_N)

    logging.info('Top N author rank')
    topAuthor(TOP_N)

    logging.info('End')
