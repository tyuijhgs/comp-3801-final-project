#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Use parser to create article graph
#
# Author: Peter Kostylev

import pdb
import time
import logging

from author_graph_builder import build_basic_graphs, build_adjacancy_graph
from CreateArticleGraph import article_adjacancy_in
from CalculateHIndex import calculateHIndex

from Utils import *
from Settings import *

def updateMiddleData():
    logging.info("Updating using " + str(MAX_DB_EXTRACT) + " articles")
    logging.info("Running build_basic_graphs")
    (authors_in_article,author_cited) = build_basic_graphs()

    logging.info("Running build_adjacancy_graph")
    build_adjacancy_graph(authors_in_article,author_cited)
    del authors_in_article
    del author_cited
    
    logging.info("Running article_adjacancy_in")
    article_adjacancy_in()

    logging.info("Running calculateHIndex")
    calculateHIndex()

    return

def main():
    """ Main program """
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
    
    timer = Timer()
    logging.info("Start")

    updateMiddleData()

    logging.info("End, it took: {0}".format(timer.delta()))

    return

if __name__ == "__main__":
    main()
