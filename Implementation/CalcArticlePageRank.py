#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Use parser to create article graph
#
# Author: Peter Kostylev

import pdb
import time
import logging
import argparse

from UpdateMiddleData import updateMiddleData
from CitationRank import Adjacancy_graph, pagerank
from author_graph_builder import *
from pprint import pprint
from DBParser import Parser
from Utils import *
from Settings import *

def calcArticlePageRank(articleAdjacency, biasVector = None):
    
    # Create an adjacency graph
    logging.info("Create adjacency graph")
    adjGraph = Adjacancy_graph(articleAdjacency, biasVector)

    # Calculate page rank
    logging.info("Run pagerank")
    steadyStateVector = pagerank(adjGraph)

    rankMap = getKeyToRankDict(steadyStateVector, adjGraph)
    
    return rankMap

def updateAuthors():
    logging.info('Update author graph')
    build_author_graph_vector()

def main():
    """ Main program """
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
    
    timer = Timer()
    logging.info("Start")

    # Add arguments and parse them
    parser = argparse.ArgumentParser(description='Calculate page rank of article graph')
    parser.add_argument('-d', action='store_true', dest='isDefault', help='Calculate pagerank using default vector (teleportation)')
    parser.add_argument('-aur', action='store_true', dest='isAuthor', help='Calculate pagerank of article rank)')
    parser.add_argument('-hi', action='store_true', dest='isHIndex', help='Calculate pagerank using hIndex vector (teleportation)')

    parser.add_argument('-u', action='store_true', dest='isUpdate', help='Update to default DB')

    parser.add_argument('-pra', action='store_true', dest='isPRAuthor', help='Calculate pagerank of article using author rank (teleportation))')

    parser.add_argument('-max', action='store_true', dest='isMax', help='Use max on authors inside articles')
    parser.add_argument('-min', action='store_true', dest='isMin', help='Use min on authors inside articles')
    parser.add_argument('-avg', action='store_true', dest='isAvg', help='Use avg on authors inside articles')
    args = parser.parse_args()

    # updates the data base
    if (args.isUpdate):
        logging.info('Updating basic info')
        updateMiddleData()

    # Load adjacency list
    logging.info("Load article adjacency matrix inverse")
    fullArticleAdjacency = loadFromJsonFile(FILE_ARTICLE_ADJECENCY_IN)
    articleAdjacency = {}

    logging.info("Filter article adjacency to " + str(MAX_ITEMS_FOR_PAGERANK) + " articles")
    if MAX_ITEMS_FOR_PAGERANK == float("inf"):
        articleAdjacency = fullArticleAdjacency
    else:
        for key in fullArticleAdjacency.keys()[:MAX_ITEMS_FOR_PAGERANK]:
            articleAdjacency[key] = fullArticleAdjacency[key]
    del fullArticleAdjacency
    articleAdjacency = addTeleportNode(articleAdjacency)

    # Load default vector
    if (args.isDefault):
        logging.info("Running using default teleportation vector")

        defaultRank = calcArticlePageRank(articleAdjacency)

        vectorToCSV(defaultRank,FILE_ARTICLE_DEFAULT_STATE)

        logging.info("Saving steady state articles using default teleportation vector:")
        logging.info(FILE_ARTICLE_DEFAULT_STATE)
        saveToJsonFile(FILE_ARTICLE_DEFAULT_STATE, defaultRank)

    authorFunc = avg
    if (args.isMax):
        logging.info("Max selected")
        praSaveFile = FILE_PAGE_RANK_AUTHORS_MAX_VECTOR
        hiSaveFile = FILE_ARTICLE_HINDEX_MAX_STATE
        authorFunc = max
    if (args.isAvg):
        logging.info("Avg selected")
        praSaveFile = FILE_PAGE_RANK_AUTHORS_AVG_VECTOR
        hiSaveFile = FILE_ARTICLE_HINDEX_AVG_STATE
        authorFunc = avg
    if (args.isMin):
        logging.info("Min selected")
        praSaveFile = FILE_PAGE_RANK_AUTHORS_MIN_VECTOR
        hiSaveFile = FILE_ARTICLE_HINDEX_MIN_STATE
        authorFunc = min

    if (args.isAuthor):
        logging.info("Author Selected")
        updateAuthors()

    # Load author vector
    if (args.isPRAuthor):
        logging.info("PR Author Selected")
        build_article_vector_base_authors(authorFunc,praSaveFile, articleAdjacency)
    
    # Load hIndex vector
    if (args.isHIndex):
        logging.info("Running using h-index teleportation vector")

        # Load resources
        logging.info("Load hIndex vector")
        hIndexVec = loadFromJsonFile(FILE_AUTHOR_H_INDEX)
        logging.info("Load atricle_id to authors")
        articleAuthors = loadFromJsonFile(FILE_MAPPING_ARTICLE_ID_TO_AUTHOR)
        
        # Map vector using the list
        logging.info("Create bias vector")
        biasVector = vector_mapping_factory(articleAuthors,hIndexVec,authorFunc)
        del hIndexVec
        del articleAuthors

        hindexRank = calcArticlePageRank(articleAdjacency, biasVector)

        logging.info("Saving steady state articles using hindex teleportation vector:")
        logging.info(hiSaveFile)
        saveToJsonFile(hiSaveFile, hindexRank)
        vectorToCSV(hindexRank,hiSaveFile)
    
    logging.info("End, it took: {0}".format(timer.delta()))

    return None

if __name__ == "__main__":
    main()
