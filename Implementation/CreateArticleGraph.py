#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Use parser to create article graph
#
# Author: Peter Kostylev

import pdb
import time
import logging
from pprint import pprint
from DBParser import Parser
from Utils import *
from Settings import *

def article_adjacancy_in():
    p = Parser()
    p.openFile()
    
    articleAdjacancyDict = {}
    
    logging.info("Building article adjacency martix")
    for item in p.parsedItems():
        if not (item["id"] in articleAdjacancyDict):
            articleAdjacancyDict[item["id"]] = []
        articleAdjacancyDict[item["id"]] += item["citations"]
        
    p.closeFile()
    
    logging.info("Saving article adjacency martix:")
    logging.info(FILE_ARTICLE_ADJECENCY_OUT)
    saveToJsonFile(FILE_ARTICLE_ADJECENCY_OUT, articleAdjacancyDict)
    
    logging.info("Inversing article adjacency matrix")
    inverseAdjMatrix = inverseAdjacencyMatrix(articleAdjacancyDict)
    del articleAdjacancyDict
    
    logging.info("Saving article adjacency martix inverse:")
    logging.info(FILE_ARTICLE_ADJECENCY_IN)
    saveToJsonFile(FILE_ARTICLE_ADJECENCY_IN, inverseAdjMatrix)

    return  inverseAdjMatrix

def main():
    """ Main program """
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
    
    timer = Timer()
    logging.info("Start")

    article_adjacancy_in()

    logging.info("End, it took: {0}".format(timer.delta()))

if __name__ == "__main__":
    main()
