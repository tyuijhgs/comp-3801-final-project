# Generates a random web link structure, and finds the
# corresponding PageRank vector.  The number of inbound
# links for each page is controlled by a power law
# distribution.
#
# This code should work for up to a few million pages on a
# modest machine.
#
# Written and tested using Python 2.5.

import numpy
import random
import pdb
from pprint import pprint
from Utils import *
from Settings import *

class web:
    def __init__(self,n):
        self.size = n
        self.in_links = {}
        self.number_out_links = {}
        self.dangling_pages = {}
        for j in xrange(n):
            self.in_links[j] = []
            self.number_out_links[j] = 0
            # we do not want this functionality
            #self.dangling_pages[j] = True

''' 
Purpose  : 
        computes a graph based on a input graph
IN :
        mapping : an adjacency matrix
        b: a vector contaning ALL vertecies! MUST BE NORMALIZED!
return :
        class of type 'web' 
'''
class Adjacancy_graph:
    def __init__(self,mapping,b=None):
        self.size = len(mapping)
        self.in_links = {}
        #self.number_out_links = {}
        self.number_out_links = numpy.matrix(numpy.zeros((len(mapping),1)))
        self.dangling_pages = {}
        self.number_of_dangaling_nodes = 0
        self.private_mapping = {}

        # Set to a vector of 0 or 1/n
        if (b == None):
            self.bias_vector = numpy.matrix(numpy.ones((len(mapping),1)))/len(mapping)
        else:
            self.bias_vector = numpy.matrix(numpy.zeros((len(mapping),1)))/len(mapping)

        # create a private mapping that will have an interface, initialize the bias vector
        for key ,i in zip(mapping.iterkeys(),xrange(self.size)):
            self.private_mapping[key] = i
            self.in_links[i] = []

            if not b == None:
                # Set to the value of b, or 0 if doesnt exists
                self.bias_vector[i] = b.get(key, 0)

        # Normalize bias_vector
        if not (b == None):
            bvSum = self.bias_vector.sum()
            for index in range(self.bias_vector.size):
                if bvSum == 0:
                    self.bias_vector[index] = 1.0 / self.bias_vector.size
                else:
                    self.bias_vector[index] = self.bias_vector[index]/bvSum

        # rename the out-nodes to proper ids
        for key,edges in mapping.iteritems():
            for edge_node in edges:
                if self.private_mapping.get(edge_node,False):
                    self.in_links[self.private_mapping[key]].append(self.private_mapping[edge_node])        
                else:
                    self.number_of_dangaling_nodes +=1

        # count the number of out_links
        for node,edges in self.in_links.iteritems():
            self.in_links[node] = edges
            for edge_node in edges:
                #self.number_out_links[edge_node] = self.number_out_links.get(edge_node,0) + 1
                self.number_out_links[edge_node] += 1

    def map_to_index(self,value):
        return self.private_mapping[value]

    def mapped_keys(self):
        return self.private_mapping.iterkeys()

def paretosample(n,power=2.0):
    '''Returns a sample from a truncated Pareto distribution
    with probability mass function p(l) proportional to
    1/l^power.  The distribution is truncated at l = n.'''
    m = n+1
    while m > n: m = numpy.random.zipf(power)
    return m

def random_web(n=1000,power=2.0):
    '''Returns a web object with n pages, and where each
    page k is linked to by L_k random other pages.  The L_k
    are independent and identically distributed random
    variables with a shifted and truncated Pareto
    probability mass function p(l) proportional to
    1/(l+1)^power.'''
    g = web(n)
    for k in xrange(n):
        lk = paretosample(n+1,power)-1
        values = random.sample(xrange(n),lk)
        g.in_links[k] = values
        pprint.pprint(values)
        for j in values: 
            #if g.number_out_links[j] == 0: g.dangling_pages.pop(j)
            g.number_out_links[j] += 1
    pprint.pprint(g.in_links)
    pprint.pprint(g.number_out_links)
    return g

def step(g,p,s=0.85):
    '''Performs a single step in the PageRank computation,
    with web g and parameter s.  Applies the corresponding M
    matrix to the vector p, and returns the resulting
    vector.'''
    n = g.size
    v = numpy.matrix(numpy.zeros((n,1)))
    inner_product = sum([p[j] for j in g.dangling_pages.keys()])
    for j in xrange(n):
        v[j] = s*sum([p[k]/g.number_out_links[k] for k in g.in_links[j]])+s*inner_product/n+(1-s)/n
    # We rescale the return vector, so it remains a
    # probability distribution even with floating point
    # roundoff.
    return v/numpy.sum(v)  

def step2(g,p,b,s=0.85):
    '''Performs a single step in the PageRank computation,
    with web g and parameter s.  Applies the corresponding M
    matrix to the vector p, and returns the resulting
    vector.'''
    n = g.size
    v = numpy.matrix(numpy.zeros((n,1)))
    #inner_product = sum([p[j] for j in g.dangling_pages.keys()])
    for j in xrange(n):
        #v[j] = s*sum([p[k]/g.number_out_links[k] for k in g.in_links[j]])+s*inner_product/n+(1-s)*b[j]
        v[j] = s*sum([p[k]/g.number_out_links[k] for k in g.in_links[j]])+(1-s)*b[j]
    # We rescale the return vector, so it remains a
    # probability distribution even with floating point
    # roundoff.
    return v/numpy.sum(v)  




def pagerank(g,s=0.85,tolerance=0.00001):
    timer = Timer()
    '''Returns the PageRank vector for the web g and
    parameter s, where the criterion for convergence is that
    we stop when M^(j+1)P-M^jP has length less than
    tolerance, in l1 norm.'''
    n = g.size
    p = numpy.matrix(numpy.ones((n,1)))/n
    iteration = 1
    change = 2
    with open(FILE_PR_TOLARANCE,'a') as f:
        f.write("{0},{1},{2},{3}\n".format("Time","Iteration","timeDelta","Change"))
    while change > tolerance:
        delta = timer.delta()
        with open(FILE_PR_TOLARANCE,'a') as f:
            f.write("{0},{1},{2},{3}\n".format(getCurrentTimeString(), iteration,delta,change))
        print "{1} : Iteration: {0} , Change In Tolerance : {2}".format(iteration,delta,change)
        new_p = step2(g,p,g.bias_vector,s)
        change = numpy.sum(numpy.abs(p-new_p))
        #print "Change in l1 norm: %s" % change
        p = new_p
        iteration += 1
    with open(FILE_PR_TOLARANCE,'a') as f:
        f.write("-!!!-END OF RUN-!!!-\n")
    return p

'''print Now computing the PageRank vector for a random
web containing 10000 pages, with the number of inbound
links to each page controlled by a Pareto power law
distribution with parameter 2.0, and with the criterion
for convergence being a change of less than 0.0001 in l1
norm over a matrix multiplication step.'''

#g = random_web(4,2.0) # works up to several million
                                                    # pages.
#pr = pagerank(g,0.85,0.0001)

if __name__ == '__main__':
    m = {1:[],'a':[1,'a'],5:[1,'a',5]}
    n = Adjacancy_graph(m)
    print pagerank(n)
