#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Use parser to create article graph
#
# Author: Peter Kostylev

import pdb
import time
import logging
from pprint import pprint
from DBParser import Parser
from Utils import *

from Settings import *

def calculateHIndex():
    # Read article graph (in)
    logging.info("Load atricle_id to authors")
    articleAuthors = loadFromJsonFile(FILE_MAPPING_ARTICLE_ID_TO_AUTHOR)
    logging.info("Load article adjacency IN")
    articleAdjacency = loadFromJsonFile(FILE_ARTICLE_ADJECENCY_IN)
    
    logging.info("Start processing data")
    
    # {author_id : {paper_id : # citations to this paper}}
    authorPapersCiteCount = {}
    
    for artId in articleAdjacency:
        for author in articleAuthors.get(artId, []):
            # If no dict for that author, create it
            papersDict = authorPapersCiteCount.get(author, {})
            # Set the count of citations to that article
            papersDict[artId] = len(articleAdjacency[artId])
            authorPapersCiteCount[author] = papersDict
    
    del articleAuthors
    del articleAdjacency
    logging.info("Start extracting h-index")
    hIndex = {}
    max = 0
    auth = "Unknown"
    
    # Extract h-index for each author
    for author in authorPapersCiteCount:
        # Lowest possible hIndex
        hIndexVal = 0
        papers = authorPapersCiteCount[author]
        # Run from most citations to the least citations papers
        for key, value in sorted(papers.iteritems(), key=lambda (k,v): (v,k), reverse=True):
            if value >= hIndexVal + 1:
                # Number of citations is grater than the position of the article
                hIndexVal += 1
            else:
                # Because it is sorted, we will never reach better hIndex for this array
                break
        if max < hIndexVal:
            max = hIndexVal
            auth = author
        hIndex[author] = hIndexVal
    
    #logging.info("Author: " + auth + " had hIndex of: " + str(max))

    logging.info("Saving author h-index dict:")
    logging.info(FILE_AUTHOR_H_INDEX)
    saveToJsonFile(FILE_AUTHOR_H_INDEX, hIndex)

    return  hIndex

def main():
    """ Main program """
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
    
    timer = Timer()
    logging.info("Start")

    calculateHIndex()

    logging.info("End, it took: {0}".format(timer.delta()))

if __name__ == "__main__":
    main()
