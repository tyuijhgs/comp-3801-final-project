import pandas
import seaborn
from Settings import *
from Utils import *

# Read in the airports data.
airports = pandas.read_csv(getCSV(FILE_PAGE_RANK_AUTHORS_MAX_VECTOR), header=None, dtype=str)
airports.columns = ['article','rank']
print airports['rank'].head()
#seaborn.distplot(airports['rank'])


# Read in the airlines data.
#airlines = pandas.read_csv("airlines.csv", header=None, dtype=str)
#airlines.columns = ["id", "name", "alias", "iata", "icao", "callsign", "country", "active"]
# Read in the routes data.
#routes = pandas.read_csv("routes.csv", header=None, dtype=str)
#routes.columns = ["airline", "airline_id", "source", "source_id", "dest", "dest_id", "codeshare", "stops", "equipment"]


