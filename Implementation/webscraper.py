import logging as logger
import requests
from bs4 import BeautifulSoup
from time import sleep
from multiprocessing import pool

#constants

def HEADER():
    return {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}

#init functions

def init_logger():
    logger.basicConfig(
        format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logger.FileHandler("helloworld.log"),
            logger.StreamHandler()
        ],
        level=logger.DEBUG 
        )

def parse(url):
    headers     = HEADER()    
    html        = None
    links       = None
    timeout     = 10

    try:
        r = requests.get(url,headers=headers,timeout=timeout)

        if r.status_code == 200:
            html = r.text
            soup = BeautifulSoup(html,'lxml')
    except Exception as ex:
        logger.warning(str(ex))

    return url

def get_refrences(url):
    return url

init_logger()
parse('https://en.wikipedia.org/wiki/Trolley_problem')

