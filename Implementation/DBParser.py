#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create a parser to parse a.miner db of citations
#
# Author: Peter Kostylev

# #* --- paperTitle
# #@ --- Authors
# #t ---- Year
# #c  --- publication venue
# #index 00---- index id of this paper
# #% ---- the id of references of this paper (there are multiple lines, with each indicating a reference)
# #! --- Abstract

import pdb
from pprint import pprint
from Settings import *

class Parser:

	def __init__(self, fPath = DB_PATH, maxDB = MAX_DB_EXTRACT):
		self.fPath = fPath
		self.maxDB = maxDB
		self.count = 0
	
	def openFile(self):
		self.f = open(self.fPath, "r")
	
	def closeFile(self):
		self.f.close()
	
	# Generator
	def parsedItems(self):
		# Assume:
		# "\n" ends an item
		# "" - meaning we reached EOF
		line = self.f.readline()
		while line != "" and self.count < self.maxDB:
			item = {"citations": []}
			isAbstract = False
			
			while line != "\n" and line != "":
				# Parse line
				symbol = line[:2]
				
				if isAbstract:
					item["abstract"] += line.strip()
					line = self.f.readline()
					continue
				
				if symbol == "#*":
					# Title
					item["title"] = line[2:].strip()
				elif symbol == "#@":
					# Authors
					item["authors"] = map(str.strip,line[2:].strip().split(","))
				elif symbol == "#t":
					# Year
					item["year"] = line[2:].strip()
				elif symbol == "#c":
					# Publication Venue
					item["pubVenue"] = line[2:].strip()
				elif symbol == "#i":
					# Index Id
					item["id"] = line[6:].strip()
				elif symbol == "#%":
					# References
					item["citations"].append(line[2:].strip())
				elif symbol == "#!":
					# Abstract
					item["abstract"] = line[2:].strip()
					isAbstract = True
				
				line = self.f.readline()

			self.count += 1
			yield item
			line = self.f.readline()
	

def main():
	""" Main program """
	
	print "Start"
	
	p = Parser(DB_PATH)
	p.openFile()
	
	for item in p.parsedItems():
		pprint.pprint(item)
		print "~"*30
		
	p.closeFile()
	return 0

if __name__ == "__main__":
	main()
