import pdb
import time
import datetime
import json
import math
import csv
from pprint import pprint

class Timer():

    def __init__(self):
        self.start = datetime.datetime.now()

    def delta(self):
        temp = self.start 
        self.start = datetime.datetime.now()
        return self.start - temp

def getCurrentTimeString():
    return datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

def inverseAdjacencyMatrix(matrix):
    newAdjacency = {}
    for item in matrix:
        # Assign default value if item doesnt exit in new dict
        newAdjacency[item] = newAdjacency.get(item, set())
        for other in matrix[item]:
            if (other in matrix):
                # Create new set if key doesnt exists
                newAdjacency[other] = newAdjacency.get(other, set())
                # Add the item to the set
                newAdjacency[other].add(item)
    
    # Convert to lists
    for key in newAdjacency:
        newAdjacency[key] = list(newAdjacency[key])
    
    return newAdjacency

def saveToJsonFile(filename, dataStructure):
    with open(filename,'wb') as f:
        json.dump(dataStructure, f)
        
def loadFromJsonFile(filename):
    with open(filename,'rb') as f:
        dataStructure = json.load(f)
    return dataStructure

'''
Retrive back the ids after we calculated steady state.
In:
    pageRankVector - the output vector of pagerank in CitationRank
    adjGraphObj - the graph object from Adjacancy_graph in CitationRank
Out:
    Map from ids to the rank corresponding to it.
'''
def getKeyToRankDict(pageRankVector, adjGraphObj):
    return {key:pageRankVector.item(adjGraphObj.map_to_index(key)) for key in adjGraphObj.mapped_keys() }

'''
Input : A_mapped_B : {Set_A:Set_B}
Input : A_vector   : {Set_A:A_RANK}
Input : funct      :lambda of a list reduce function
'''
def vector_mapping_factory(A_mapped_B,A_vector,funct=max):
    vector = dict()

    for key, item in A_mapped_B.iteritems():
        vector[key] = funct([A_vector.get(i,0) for i in item])

    # TODO: Can just return vector, pagerank will normalize anyway
    return vector_to_probability(vector)

def vector_to_probability(vector):
    s = float(sum(vector.values()))

    # All the values are 0
    if s == 0:
        # Set all items to have same probability
        s = len(vector)
        return {key:1.0/s for key,item in vector.iteritems()}
    
    # Devide all values by the sum to have sum(vecor) = 1
    return {key:item/s for key,item in vector.iteritems()}

'''
A : input : mapping to float
B : input : mapping to flaot
Description : A and B should have same mappings, returns the non-normalized cosine distance 1 if similar, 0 if not
'''
def cosineDistance(A,B):
    A_d = math.sqrt(sum([value*value for key,value in A.iteritems()]))*math.sqrt(sum([value*value for key,value in B.iteritems()]))
    return sum([A.get(key,0)*B.get(key,0) for key,value in A.iteritems()])/A_d


'''
avg : takes a list and returns the average of the list
l : INPUT : list
'''
def avg(l):
    if len(l) == 0:
        return 0
    return float(sum(l))/float(len(l))

'''
HAM : calculates the HAMMING DISTANCE BETWEEN THE TWO
A : INPUT : dict()
B : INPUT : dict()
'''
def HAM(A,B):
    Ap = sorted(A.items(),key=lambda x:x[1])
    Bp = sorted(B.items(),key=lambda x:x[1])
    
    c = 0
    for a, b in zip(Ap,Bp):
        if a[0] != b[0]:
            c += 1
    return c

def getCSV(FILE):
    return FILE + '.csv'

def vectorToCSV(v,OUTPUT_FILE,headers=['unknown']):
    vector = sorted(v.items(),key=lambda x:x[1])

    with open(getCSV(OUTPUT_FILE),'w') as f: 
        writer = csv.writer(f)
        #writer.writerow(headers)
        for key, item in vector:
            writer.writerow([key,item])


def combinations(iterable, r):
    # combinations('ABCD', 2) --> AB AC AD BC BD CD
    # combinations(range(4), 3) --> 012 013 023 123
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = list(range(r))
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)

def addTeleportNode(adjGraph):
    # For each node
    # Remove outside connections
    # Check if empty edges
    # Add d to the edge
    teleportNode = "111111"
    teleportEdges = []

    for node, edges in adjGraph.iteritems():
        teleportEdges.append(node)
        newEdges = []
        for edge_node in edges:
            if adjGraph.get(edge_node,False):
                newEdges.append(edge_node)
        if len(newEdges) == 0:
            newEdges.append(teleportNode)
        adjGraph[node] = newEdges
    adjGraph[teleportNode] = teleportEdges

    return adjGraph





