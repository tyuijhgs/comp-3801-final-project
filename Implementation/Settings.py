#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Use parser to create article graph
#
# Author: Peter Kostylev

# Test DB path
#DB_PATH = "./DB/Test.db.txt"
DEBUG = False
DEBUG_DB = False
UPDATE = True
MAX_DB_EXTRACT = float('inf')
MAX_ITEMS_FOR_PAGERANK = 2500000
VECTOR_LIST = []
TOP_N = 100000

# Test DB path
# Peter paths
DB_PATH = "D:\Peter\Workplace\COMP3801\citation-acm-v8.txt"
#DB_PATH = "C:\Peter\University\COMP3801\Project\citation-acm-v8.txt\citation-acm-v8.txt"

# Chris paths
#DB_PATH = '/home/christopherblackman/Downloads/aminer.org/citation-acm-v8.txt'
#DB_PATH = '/home/tyuijhgs/citation-acm-v8.txt'
if DEBUG_DB:
    DB_PATH = "./DB/Test.db.txt"
    #DB_PATH = "C:\Peter\University\COMP3801\Project\citation-acm-v8.txt\citation-acm-v8.txt"

# {article_a : title (str)}
FILE_ARTICLE_ID_TO_TITLE_MAP = 'DB/article_id_to_title.json'
if DEBUG:
    FILE_ARTICLE_ID_TO_TITLE_MAP += '.debug'


# {article_a : [article_id that article_a cited]}
FILE_ARTICLE_ADJECENCY_OUT = 'DB/article_adjacency_out.json'
if DEBUG:
    FILE_ARTICLE_ADJECENCY_OUT += '.debug'

# {article_a : [article_id that cited article_a]}
FILE_ARTICLE_ADJECENCY_IN = 'DB/article_adjacency_in.json'
if DEBUG:
    FILE_ARTICLE_ADJECENCY_IN += '.debug'

# H-Index per author: {author_id: hIndex value}
FILE_AUTHOR_H_INDEX = 'DB/author_h_index.json'
if DEBUG:
    FILE_AUTHOR_H_INDEX += '.debug'

# Article graph steady state using default vector: {article_id: rank}
FILE_ARTICLE_DEFAULT_STATE = 'DB/article_default_state.json'
if DEBUG:
    FILE_ARTICLE_DEFAULT_STATE += '.debug'
VECTOR_LIST.append(FILE_ARTICLE_DEFAULT_STATE)

# Article graph steady state using hIndex vector: {article_id: rank}
FILE_ARTICLE_HINDEX_AVG_STATE = 'DB/article_hindex_avg_state.json'
if DEBUG:
    FILE_ARTICLE_HINDEX_AVG_STATE += '.debug'
VECTOR_LIST.append(FILE_ARTICLE_HINDEX_AVG_STATE)

# Article graph steady state using hIndex vector: {article_id: rank}
FILE_ARTICLE_HINDEX_MIN_STATE = 'DB/article_hindex_min_state.json'
if DEBUG:
    FILE_ARTICLE_HINDEX_MIN_STATE += '.debug'
VECTOR_LIST.append(FILE_ARTICLE_HINDEX_MIN_STATE)

# Article graph steady state using hIndex vector: {article_id: rank}
FILE_ARTICLE_HINDEX_MAX_STATE = 'DB/article_hindex_max_state.json'
if DEBUG:
    FILE_ARTICLE_HINDEX_MAX_STATE += '.debug'
VECTOR_LIST.append(FILE_ARTICLE_HINDEX_MAX_STATE)
# {Article id : [authors who are in article]}
FILE_MAPPING_ARTICLE_ID_TO_AUTHOR = 'DB/article_id_to_author_mapping.json'
if DEBUG:
    FILE_MAPPING_ARTICLE_ID_TO_AUTHOR += '.debug'

# {Author_a : [article that the author wrote]}
FILE_MAPPING_AUTHOR_TO_ARTICLE = 'DB/author_to_article_mapping.json'
if DEBUG:
    FILE_MAPPING_AUTHOR_TO_ARTICLE += '.debug' 

# {author_a : [authors that got cited by author_a]}
FILE_ADJACANCY_LIST_AUTHORS = 'DB/adjacency_list_authors.json'
if DEBUG:
    FILE_ADJACANCY_LIST_AUTHORS += '.debug'

# Page rank of authors
FILE_AUTHOR_VECTOR = 'DB/author_citation_vector.json'
if DEBUG:
    FILE_AUTHOR_VECTOR += '.debug'

# Page rank of articles with author vector applied a bias
FILE_PAGE_RANK_AUTHORS_MAX_VECTOR = 'DB/article_author_bias_MAX.json'
if  DEBUG:
    FILE_PAGE_RANK_AUTHORS_MAX_VECTOR += '.debug'
VECTOR_LIST.append(FILE_PAGE_RANK_AUTHORS_MAX_VECTOR)

FILE_PAGE_RANK_AUTHORS_MIN_VECTOR = 'DB/article_author_bias_MIN.json'
if  DEBUG:
    FILE_PAGE_RANK_AUTHORS_MIN_VECTOR += '.debug'
VECTOR_LIST.append(FILE_PAGE_RANK_AUTHORS_MIN_VECTOR)

FILE_PAGE_RANK_AUTHORS_AVG_VECTOR = 'DB/article_author_bias_AVG.json'
if  DEBUG:
    FILE_PAGE_RANK_AUTHORS_AVG_VECTOR += '.debug'
VECTOR_LIST.append(FILE_PAGE_RANK_AUTHORS_AVG_VECTOR)

# testing

FILE_PR_TOLARANCE =  'DB/page-rank-default.csv'
FILE_PR_TOLARANCE =  'DB/page-rank-max-author.csv'
FILE_PR_TOLARANCE =  'DB/page-rank-min-author.csv'
FILE_PR_TOLARANCE =  'DB/page-rank-avg-author.csv'
FILE_PR_TOLARANCE =  'DB/page-rank-max-hindex.csv'
FILE_PR_TOLARANCE =  'DB/page-rank-min-hindex.csv'
FILE_PR_TOLARANCE =  'DB/page-rank-avg-hindex.csv'

FILE_CSV_COSINE =  'DB/cosine.csv'
FILE_CSV_INDEX_MATCH_MAX =  'DB/index_match_max.csv'
FILE_CSV_INDEX_MATCH_MIN =  'DB/index_match_min.csv'
FILE_CSV_INDEX_MATCH_AVG =  'DB/index_match_avg.csv'

FILE_HINDEX_TOP_N = 'DB/hIndex_top_n.csv'
FILE_AUTHOR_TOP_N = 'DB/author_top_n.csv'




