# Steps of the project:

First:
1. Parser

Dev1:

1. extract h-index.
    - Sort all papers of author_a by the number of times each paper was cited in other papers
    - h-index = last i, fi(P) <= # cited(P)
    - h-index = last position in which f is greater or equal to the position.
2. Article graph
3. page rank on article graph using 1/n vector (teleport + V_0)
	B = 0.85
4. Page rank on article graph using h-index vector (teleport + V_0)
	B = 0.85

Dev2:

1. Author graph
2. Page rank on author graph using 1/n vector (teleport + V_0)
	B = 0.85
3. Page rank on article graph using steady state
	B = 0.85
    
# Creating graphs:
- OpenOrd
- ForceAtlas 2
- Radial Axis
- GeoLayout

* Invent your own approach and compare them.
* devide by approaches
* Look at the data as a plot or a graph
* google scolar

# Useful info:
- approaches
- markov
- Google search terms
- Use h-index

# From lectures:
- density in cluster
- tags / references
- compare to data sets
- google scholar
- amazon / SVD popularity of items
- users and the rating
- Search popularity on google


- based on some criteria
- image detection


# References 
- Mining of Massive Datasets 
	- http://www.mmds.org/
- Original paper on google page rank -- not in depth of mathematics
	- http://ilpubs.stanford.edu:8090/361/1/1998-8.pdf
- H index
	- http://subjectguides.uwaterloo.ca/calculate-academic-footprint/YourHIndex
- another shorther more mathematical artical on page rank
	- https://webserver2.tecgraf.puc-rio.br/eda/referencias/Google-petteri_huuhka_google_paper.pdf
- in depth mathimatical look into page rank
	- http://www4.ncsu.edu/~ipsen/ps/slides_dagstuhl07071.pdf
- page rank implementation / efficiency
	- http://michaelnielsen.org/blog/using-your-laptop-to-compute-pagerank-for-millions-of-webpages/
- Collection of citation database:
    - https://aminer.org/citation
- Another aproach to raning Scientific Articles
    - http://epubs.siam.org/doi/abs/10.1137/1.9781611972795.46
