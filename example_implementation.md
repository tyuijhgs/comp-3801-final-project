```Python
Article_index = dict() 
# parser loop
def iterator(current_article, list_of_citations)	
	if Article_index[current_article] = None:
		Article_index[current_article] = set()
	Article_index[current_article].add(current_article)	

	for cite in list_of_citations:
		if Article_index[cite] == None:
			Article_index[cite] = set()

		Article_index[cite].add(current_article)
```

author -> articles

articles -> authors

first find : authors -> articles

go through each article 

```Python
authors_in_article = dict() 
# contains a set as key
author_cited = dict()

#this gives me a lookup table
for article in DB:
	list_authors, article_id, citations = article
	authors_in_article[article_id] = list_authors

	for author in list_authors:
		author_cited[author].add(citations + article_id)
	
author_graph = dict(default=set())
# create author citation graph
for author in authors:
	list_of_articles = author_cited[author]
	for article in list_of_articles:
		author_graph[author].add(authors_in_article[article])

convert author_graph to a readable format

apply page rank on the author graph

Beta_vector = dict()

for article in article DB:
	list_authors, article_id, citations = article
	
	list_of_attributes
	for author in list_authors:
		list_of_attributes.append(lookup_author_P[author])

	beta_vector[article_id] = max(list_of_attributes)

normalize beta_vector
calculate page rank on citation graph with beta vector
```

